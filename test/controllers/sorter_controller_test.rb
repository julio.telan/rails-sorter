require 'test_helper'

class SorterControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get sorter_index_url
    assert_response :success
  end

  test "should get sort" do
    get sorter_sort_url
    assert_response :success
  end

end
