puts "PWD => #{Dir.pwd}"
require './app/models/sorter'

RSpec.describe Sorter do
  it 'can create a model' do
    model = Sorter.new given_string: 'abcde'
    expect(model).to_not be(nil)
    expect(model.given_string).to eq('abcde')
  end

  it 'should sort using bubblesort' do
    model = Sorter.new given_string: 'befdac'
    sorted = model.bubblesort
    expect(sorted).to eq('abcdef')
  end

  it 'should sort using quicksort' do
    model = Sorter.new given_string: 'befdac'
    sorted = model.quicksort
    expect(sorted).to eq('abcdef')
  end

  it 'should sort using quicksort with numbers' do
    model = Sorter.new given_string: 'befdac123'
    sorted = model.quicksort
    expect(sorted).to eq('123abcdef')
  end

  it 'should perform the chosen method' do
    model = Sorter.new given_string: 'bedfac'
    sorted = model.sort_by('quicksort')
    expect(sorted).to eq('abcdef')
  end
end

