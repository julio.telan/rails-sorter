class SorterController < ApplicationController
    def index; end

    def sort
        given_string = params['given_string']
        method = params['method']
        model = Sorted.new given_string
        result = Sorted.sort_by(method)
        data = {
            data: {
                result: result,
                method: method,
                given_string: given_string
            }
        }
        render :json => data
    end
end
