require 'algorithms'
puts "PWD => #{Dir.pwd}"
require 'active_model'
class Sorter
    include ActiveModel::Model
    attr_accessor :given_string
    
    def bubblesort
        Algorithms::Sort.bubble_sort given_string
    end

    def quicksort
        Algorithms::Sort.quicksort given_string
    end

    def sort_by (method)
        if method == 'bubblesort'
            bubblesort
        elsif method == 'quicksort'
            quicksort
        else
            raise 'Method not recognized'
        end
    end
end
