import React from "react";
import ReactDOM from "react-dom"
import { connect } from "react-redux";
import { compose, createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunkMiddleware from "redux-thunk";
import SorterReducer from "./Sorter/reducers/sorter";
import * as SorterActions from "./Sorter/actions/sorter";

const createSorterStore = compose(
    applyMiddleware(thunkMiddleware)
)(createStore);
console.log("GHERE2")
class Sorter extends React.Component {
    componentWillMount() {
        console.log("GHERE")
        let initialState = {};
        // initialState["given_string"] = "";
        // initialState["method"] = "bubblesort";
        this.store = createSorterStore(SorterReducer, initialState);
    }

    render(){
        return (
            <div>
                TEST
            </div>
        );
    }
}

Sorter.propTypes = {
};

function mapStateToProps(state) {
    return state;
}
const VisibleSorter = connect(
    mapStateToProps,
    SorterActions
)(Sorter);

export default VisibleSorter;

document.addEventListener('DOMContentLoaded', () => {
   ReactDOM.render(
    <VisibleSorter />,
    document.body.appendChild(document.createElement('div')),
    )
})