import React from "react";
import { connect } from "react-redux";
import { compose, createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunkMiddleware from "redux-thunk";
import SorterReducer from "../reducers/sorter";
import * as SorterActions from "../actions/sorter";

class Sorter extends React.Component {
    render(){
        return (
            <div>
                <div>
                    <div className="col-md-8">
                        <input value={this.props.given_string} 
                            onChange={(e) => this.props.change_attribute("GIVEN_NAME", e)} />
                    </div>
                    <div className="col-md-2">
                        <select onChange={(e) => this.props.change_attribute("METHOD", e)}>
                            <option value="bubblesort">Bubble Sort</option>
                            <option value="quicksort">Quick Sort</option>
                        </select>
                    </div>
                    <div className="col-md-2">
                        <button type="button" className="button success-button" 
                            onClick={this.props.submitSort} />
                    </div>
                </div>
                <div>
                        <h4>RESULT:</h4>
                        {this.props.result}
                </div>
            </div>
        );
    }
}

Sorter.propTypes = {
};

function mapStateToProps(state) {
    return state;
}
const VisibleSorter = connect(
    mapStateToProps,
    SorterActions
)(Sorter);

export default VisibleSorter;
