import { combineReducers } from "redux";

function given_string(given_string = "", action) {
    switch (action.type) {
    case "CHANGE_GIVEN_STRING":{
        return action.value;
    }
    default:
        return given_string;
    }
}

function method(method = "bubblesort", action) {
    switch (action.type) {
    case "CHANGE_METHOD":{
        return action.value;
    }
    default:
        return method;
    }
}

function result(result = "", action) {
    switch(action.type) {
    case "RECEIVE_RESULT":
        return action.value;
    default:
        return result;
    }
}

const sorterReducer = combineReducers({
    given_string,
    method,
    result
});


export default sorterReducer;
