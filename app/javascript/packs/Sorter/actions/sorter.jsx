import axios from "axios";

function receiveResult(value) {
    return {
        type: "RECEIVE_RESULT",
        value
    }
}

export function submitSort() {
    return (dispatch, getState) => {
        return axios({
            url: '/sort',
            params: {
                method: getState().method,
                given_string: getState().given_string
            },
            timeout: 20000,
            method: 'get',
            responseType: "json"
        })
            .then((response) => {
                dispatch(receiveResult(response.data.data.result));
            })
            .catch((error) => {
                alert(error);
            });
    };
}

export function change_attribute(attr, event) {
    return {
        type: 'CHANGE_' + attr,
        value: event.target.value
    };
}