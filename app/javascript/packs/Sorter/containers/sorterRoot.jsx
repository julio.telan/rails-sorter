import { compose, createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunkMiddleware from "redux-thunk";
import VisibleSorter from "../components/sorter";
import React from "react";
import SorterReducer from "../reducers/sorter";

const createSorterStore = compose(
    applyMiddleware(thunkMiddleware)
)(createStore);

class SorterRoot extends React.Component {
    componentWillMount() {
        let initialState = {};
        initialState["given_string"] = "";
        initialState["method"] = "bubblesort";
        this.store = createSorterStore(SorterReducer, initialState);
    }

    render() {
        return (
            <Provider store={this.store}>
                <VisibleSorter />
            </Provider>
        );
    }
}

SorterRoot.propTypes = {
};

export default SorterRoot;
